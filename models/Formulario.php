<?php

namespace app\models;

use yii\base\Model;

class Formulario extends Model
{
    public ?int $activado = null;
    public ?string $mes = null;
    public ?string $opcion = null;
    public array $dias = [];
    public ?int $id = null;
    public ?string $fecha = null;
    public $archivo; // se almacena el archivo subido como objeto


    public function rules(): array
    {
        return [
            [['mes', 'opcion', 'dias', 'id'], 'required'],
            [['activado', 'fecha'], 'safe'],
            [
                ['archivo'], 'file',
                'skipOnEmpty' => false, // obligatorio seleccionas una imagen
                'extensions' => 'pdf' // extensiones permitidas
            ],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            "activado" => "Activado",
            "mes" => "Mes de reserva",
            "opcion" => "Opciones a elegir",
            "dias" => "Seleccione uno o varios dias",
            "id" => "Id",
            "fecha" => "Fecha Reserva",
            "archivo" => "Suba archivo pdf",
        ];
    }
    public function subirArchivo(): bool
    {
        $this->archivo->saveAs('pdf/' . $this->archivo->name);
        return true;
    }

    /**
     * antes de validar cojo los archivos enviados y
     * los coloco en el modelo
     * 
     */
    public function beforeValidate()
    {
        $this->archivo = \yii\web\UploadedFile::getInstance($this, 'archivo');
        return true;
    }


    /**
     * despues de validar subo el archivo
     * 
     */
    public function afterValidate()
    {
        $this->subirArchivo();
        return true;
    }

    public function meses(): array
    {
        return [
            "enero" => "Enero",
            "febrero" => "Febrero"
        ];
    }

    public function opciones(): array
    {
        return [
            "opcion 1" => "Opcion 1",
            "opcion 2" => "Opcion 2",
            "opcion 3" => "Opcion 3"
        ];
    }

    public function diasMostrar(): array
    {
        return [
            "lunes" => "Lunes",
            "martes" => "Martes"
        ];
    }

    public function getDiasseleccionados()
    {
        return join(",", $this->dias);
    }
}
