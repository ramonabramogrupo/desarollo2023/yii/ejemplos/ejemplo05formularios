<?php

use yii\helpers\Html;

foreach ($ficheros as $fichero) {
    echo "<div>";
    echo Html::a($fichero, '@web/pdf/' . $fichero, ["class" => "btn btn-primary"]);
    echo "</div><br>";
}
