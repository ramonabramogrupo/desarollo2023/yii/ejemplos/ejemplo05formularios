<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario $model */
/** @var ActiveForm $form */
?>
<div class="site-formulario">

    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form
        ->field($model, 'activado')
        ->checkbox()
    ?>
    <?=
    $form
        ->field($model, 'mes')
        ->dropDownList($model->meses())
    ?>
    <?=
    $form
        ->field($model, 'opcion')
        ->radioList($model->opciones())
    ?>
    <?=
    $form
        ->field($model, 'dias')
        ->listBox($model->diasMostrar(), [
            'multiple' => 'multiple'
        ])
    ?>
    <?=
    $form
        ->field($model, 'id')
        ->input('number')
    ?>
    <?=
    $form
        ->field($model, 'fecha')
        ->input('date')
    ?>
    <?=
    $form
        ->field($model, 'archivo')
        ->fileInput();
    ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-formulario -->