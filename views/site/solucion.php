<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'activado',
        'mes',
        'opcion',
        'id',
        'fecha',
        [
            "attribute" => 'archivo',
            "format" => "raw",
            "value" => function ($model) {
                return Html::a(
                    $model->archivo->name,
                    '@web/pdf/' . $model->archivo->name,
                    ["class" => "btn btn-primary"]
                );
            }
        ],
        "DiasSeleccionados"
    ]
]);
