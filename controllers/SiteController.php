<?php

namespace app\controllers;

use app\models\Formulario;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\FileHelper;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionFormulario()
    {
        $model = new Formulario();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render("solucion", [
                    "model" => $model
                ]);
            }
        }

        return $this->render('formulario', [
            'model' => $model,
        ]);
    }

    public function actionFicheros()
    {
        $ficheros = FileHelper::findFiles('./pdf');
        foreach ($ficheros as $indice => $fichero) {
            $ficheros[$indice] = substr($fichero, strpos($fichero, '\\') + 1);
        }

        // utilizo compact para enviar argumento como 
        // array asociativo
        return $this->render('ficheros', compact("ficheros"));
        // return $this->render('ficheros', [
        //     "ficheros" => $ficheros
        // ]);
    }
}
